package io.osscg.quaestio4j

import org.slf4j.Logger

interface Logging {
  @Suppress("PropertyName")
  val LOGGER: Logger get() = createKotlinLogger(javaClass)
}
