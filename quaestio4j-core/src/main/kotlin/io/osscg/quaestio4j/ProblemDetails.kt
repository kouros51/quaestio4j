package io.osscg.quaestio4j

import java.net.URI

data class ProblemDetails internal constructor(
  private val type: URI,
  private val title: String,
  private val status: Status,
  private val details: String?,
  private val instance: URI?
) {
  companion object {
    val DEFAULT_TYPE: URI = URI.create("about:black")

    fun builder(status: Status) = ProblemDetailsBuilder(status)

    class ProblemDetailsBuilder(private val status: Status) {
      fun build() = ProblemDetails(DEFAULT_TYPE, status.reason, status, null, null)
    }
  }

  override fun toString(): String {
    return String.format(
      "( type=%s, title=%s, status=%s, details=%s, instance=%s )",
      type,
      title,
      status,
      details,
      instance
    )
  }
}
