package io.osscg.quaestio4j

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class ProblemDetailsTest {
  @Test
  fun `Problem created with only status should only have default type and title of that status status`() {
    val problem = ProblemDetails
      .builder(Status.BAD_REQUEST)
      .build()

    assertThat(problem).isEqualTo(
      ProblemDetails(
        ProblemDetails.DEFAULT_TYPE,
        Status.BAD_REQUEST.reason,
        Status.BAD_REQUEST,
        null,
        null
      )
    )
  }
}
