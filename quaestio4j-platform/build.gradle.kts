plugins {
  `java-platform`
}

javaPlatform {
  allowDependencies()
}

val lombokVersion: String = "1.18.24"
val mockitoVersion: String = "4.6.1"
val truthVersion: String = "1.1.3"

dependencies {
  api(platform("org.junit:junit-bom:5.8.2"))

  constraints {
    // Used for production code
    api("org.slf4j:slf4j-api:1.7.36")

    // Used for testing code
    api("org.assertj:assertj-core:3.23.1")

    api("org.junit.jupiter:junit-jupiter-api")
    api("org.junit.jupiter:junit-jupiter-engine")

    api("org.mockito:mockito-core:${mockitoVersion}")
    api("org.mockito:mockito-junit-jupiter:${mockitoVersion}")
  }
}
