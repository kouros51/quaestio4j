import org.gradle.api.tasks.testing.logging.TestLogEvent.*

plugins {
  idea
  kotlin("jvm")
}

val javaVersion = JavaVersion.VERSION_17

allprojects {
  group = "io.osscg.quaestio4j"
  version = "0.0.1-SNAPSHOT"

  repositories {
    mavenLocal()
    mavenCentral()
    gradlePluginPortal()
  }

  tasks {
    withType<JavaCompile> {
      options.encoding = "UTF-8"
      sourceCompatibility = javaVersion.toString()
      targetCompatibility = javaVersion.toString()
    }
  }
}

subprojects {
  tasks.withType<Test> {
    useJUnitPlatform()

    testLogging {
      // Available events: PASSED, FAILED, SKIPPED, STANDARD_OUT, STANDARD_ERROR
      events = setOf(
        PASSED,
        FAILED,
        SKIPPED,
        STANDARD_OUT
      )
      exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
    }
  }
}
