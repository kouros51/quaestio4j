pluginManagement {
  val kotlinVersion: String by settings

  repositories {
    mavenLocal()
    mavenCentral()
    gradlePluginPortal()
  }

  plugins {
    kotlin("jvm") version kotlinVersion
  }
}


rootProject.name = "quaestio4j"

include("quaestio4j-core")
include("quaestio4j-platform")
